package onlinetours.test

import akka.actor.{ActorSystem, Props}
import akka.testkit.TestProbe
import onlinetours.test.Flights._
import onlinetours.test.util.ApiActor.{SearchStarted, StartFlightLoading, StartSearch}
import onlinetours.test.util.DbActor._
import onlinetours.test.util.ToursDbActor._
import org.joda.time.{Days, LocalDate, LocalTime}
import org.scalatest.FreeSpec

import scala.util.{Failure, Success}


class SearchActorTest extends FreeSpec {
  implicit val system: ActorSystem = ActorSystem("test")

  val today = LocalDate.now()

  trait Fixture {
    val startDate = today
    val endDate = today plusDays 5

    val departIata = "UN"
    val arrivalIata = "SU"

    def request: Request = Request(departIata, arrivalIata, startDate, endDate)

    val dbActor = TestProbe()
    val tourDbActor = TestProbe()
    val apiActor = TestProbe()
    val requester = TestProbe()

    val searchActor = system.actorOf(Props(new SearchActor(request, requester.ref, dbActor.ref, tourDbActor.ref, apiActor.ref)))

    val departureTime = LocalTime.MIDNIGHT
    val arrivalTime = LocalTime.MIDNIGHT plusHours 10

    val departCompany = "Transaero"
    val arrivalCompany = "Aeroflot"

    val aviacompanyIataByName = Map(departCompany -> departIata, arrivalCompany -> arrivalIata)

    val cheapestTour = Tour(
      tourId = "cheapest",
      hotelName = "hotel1",
      minCost = 123,
      departFlights = List(Flight(Aviacompany(departCompany, Some(departIata)), startDate.toLocalDateTime(departureTime))),
      arrivalFlights = List(Flight(Aviacompany(arrivalCompany, Some(arrivalIata)), endDate.toLocalDateTime(arrivalTime)))
    )

    val cheapestDbTourNoFlights = DbTour(
      tourId = cheapestTour.tourId,
      hotelName = cheapestTour.hotelName,
      minCost = cheapestTour.minCost,
      departFlights = None,
      arrivalFlights = None
    )

    val cheapestDbTour = cheapestDbTourNoFlights.copy(
      departFlights = Some(List(DbFlight(aviacompanyName = departCompany, date = startDate, time = Some(departureTime)))),
      arrivalFlights = Some(List(DbFlight(aviacompanyName = arrivalCompany, date = endDate, time = Some(arrivalTime))))
    )

    val departRegion = 1L
    val arrivalRegion = 2L

    val searchQuery = SearchQuery(
      departure = departRegion,
      arrival = arrivalRegion,
      startDate = startDate,
      duration = Days.daysBetween(startDate.toLocalDateTime(departureTime), endDate.toLocalDateTime(arrivalTime)).getDays
    )

    val regionByIataFetched = Map(departIata -> departRegion, arrivalIata -> arrivalRegion)

    val notCheapestTour = Tour(
      tourId = "not the cheapest",
      hotelName = "hotel2",
      minCost = 200,
      departFlights = List(Flight(Aviacompany(departCompany, Some(departIata)), startDate.toLocalDateTime(departureTime))),
      arrivalFlights = List(Flight(Aviacompany(arrivalCompany, Some(arrivalIata)), endDate.toLocalDateTime(arrivalTime)))
    )

    val notCheapestDbTourNoFlights = DbTour(
      tourId = notCheapestTour.tourId,
      hotelName = notCheapestTour.hotelName,
      minCost = notCheapestTour.minCost,
      departFlights = None,
      arrivalFlights = None
    )

    val notCheapestDbTour = notCheapestDbTourNoFlights.copy(
      departFlights = Some(List(DbFlight(aviacompanyName = departCompany, date = startDate, time = Some(departureTime)))),
      arrivalFlights = Some(List(DbFlight(aviacompanyName = arrivalCompany, date = endDate, time = Some(arrivalTime))))
    )

    val searchId = "SearchId"
  }

  "processes request and reply with the cheapest tour" in new Fixture {
    val dbTourCacheNoFlights = notCheapestDbTourNoFlights
    val dbTourCacheWithFlights = notCheapestDbTour
    val dbTourSearchNoFlights = cheapestDbTourNoFlights
    val dbTourSearchWithFlights = cheapestDbTour

    dbActor.expectMsg(FetchRegionIdByIata(Set(request.arrivalIata, request.departureIata)))
    dbActor.reply(RegionByIataFetched(regionByIataFetched))

    tourDbActor.expectMsg(FindCheapestTour(searchQuery))
    tourDbActor.reply(Cheapest(Some(dbTourCacheNoFlights)))

    apiActor.expectMsg(StartSearch(searchQuery))
    apiActor.reply(SearchStarted(searchId))

    apiActor.expectMsg(StartFlightLoading(dbTourCacheNoFlights.tourId))

    tourDbActor.expectMsg(FindCheapestTour(searchQuery))
    tourDbActor.reply(Cheapest(Some(dbTourSearchNoFlights)))

    tourDbActor.expectMsg(FetchTour(dbTourCacheNoFlights.tourId))
    tourDbActor.reply(TourFetched(Some(dbTourCacheWithFlights)))

    dbActor.expectMsg(FetchSearchStatus(searchId))
    dbActor.reply(SearchStatus(searchId, true, 3))

    apiActor.expectMsg(StartFlightLoading(dbTourSearchNoFlights.tourId))

    dbActor.expectMsg(FetchAviacompanyIataByName(Set(departCompany, arrivalCompany)))
    dbActor.reply(AviacompanyIataByNameFetched(aviacompanyIataByName))

    tourDbActor.fishForMessage() {
      case FindCheapestTour(_) => false
      case FetchTour(dbTourSearchNoFlights.tourId) => true
    }
    tourDbActor.reply(TourFetched(Some(dbTourSearchWithFlights)))

    dbActor.expectMsg(FetchAviacompanyIataByName(Set(departCompany, arrivalCompany)))
    dbActor.reply(AviacompanyIataByNameFetched(aviacompanyIataByName))

    requester.expectMsg(Response(Success(cheapestTour)))
  }

  "reply with error if iata-code not found" in new Fixture {
    override def request: Request = Request("UNDEFINED", "SSH", startDate, endDate)

    dbActor.expectMsg(FetchRegionIdByIata(Set(request.arrivalIata, request.departureIata)))
    dbActor.reply(RegionByIataFetched(Map("SSH" -> 1)))

    requester.expectMsgPF() {
      case Response(Failure(ex))
        if ex.isInstanceOf[RuntimeException] && ex.getMessage == "Iata codes not found: UNDEFINED" => true
    }
  }

  "reply with tour from cache if search doesn't return any results" in new Fixture {
    val dbTourCache = notCheapestDbTour

    dbActor.expectMsg(FetchRegionIdByIata(Set(request.arrivalIata, request.departureIata)))
    dbActor.reply(RegionByIataFetched(regionByIataFetched))

    tourDbActor.expectMsg(FindCheapestTour(searchQuery))
    tourDbActor.reply(Cheapest(Some(dbTourCache)))

    apiActor.expectMsg(StartSearch(searchQuery))
    apiActor.reply(SearchStarted(searchId))

    dbActor.expectMsg(FetchAviacompanyIataByName(Set(departCompany, arrivalCompany)))
    dbActor.reply(AviacompanyIataByNameFetched(aviacompanyIataByName))

    dbActor.expectMsg(FetchSearchStatus(searchId))
    dbActor.reply(SearchStatus(searchId, true, 0))

    requester.expectMsg(Response(Success(notCheapestTour)))
  }

  "reply with tour from cache if search is not completed in the specified time" in new Fixture {
    val dbTourCache = notCheapestDbTour

    dbActor.expectMsg(FetchRegionIdByIata(Set(request.arrivalIata, request.departureIata)))
    dbActor.reply(RegionByIataFetched(regionByIataFetched))

    tourDbActor.expectMsg(FindCheapestTour(searchQuery))
    tourDbActor.reply(Cheapest(Some(dbTourCache)))

    apiActor.expectMsg(StartSearch(searchQuery))
    apiActor.reply(SearchStarted(searchId))

    dbActor.expectMsg(FetchAviacompanyIataByName(Set(departCompany, arrivalCompany)))
    dbActor.reply(AviacompanyIataByNameFetched(aviacompanyIataByName))

    requester.expectMsg(SearchActor.RequestTimeout, Response(Success(notCheapestTour)))
  }

  "if getting information about flights not completed within the specified time, reply with the cheapest tour with already known flights" in new Fixture {
    val dbTourCache = notCheapestDbTour
    val dbTourSearchNoFlights = cheapestDbTourNoFlights

    dbActor.expectMsg(FetchRegionIdByIata(Set(request.arrivalIata, request.departureIata)))
    dbActor.reply(RegionByIataFetched(regionByIataFetched))

    tourDbActor.expectMsg(FindCheapestTour(searchQuery))
    tourDbActor.reply(Cheapest(Some(dbTourCache)))

    apiActor.expectMsg(StartSearch(searchQuery))
    apiActor.reply(SearchStarted(searchId))

    dbActor.expectMsg(FetchAviacompanyIataByName(Set(departCompany, arrivalCompany)))
    dbActor.reply(AviacompanyIataByNameFetched(aviacompanyIataByName))

    dbActor.expectMsg(FetchSearchStatus(searchId))
    dbActor.reply(SearchStatus(searchId, true, 1))

    tourDbActor.expectMsg(FindCheapestTour(searchQuery))
    tourDbActor.reply(Cheapest(Some(dbTourSearchNoFlights)))

    apiActor.expectMsg(StartFlightLoading(dbTourSearchNoFlights.tourId))

    requester.expectMsg(SearchActor.RequestTimeout, Response(Success(notCheapestTour)))
  }
}
