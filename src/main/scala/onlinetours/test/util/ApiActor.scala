package onlinetours.test.util

/**
 * Performs an API site call that allows you to run search, or start downloading the detailed information about flights
 * for the selected tour
 */
import akka.actor.Actor
import onlinetours.test.Flights.SearchQuery

class ApiActor extends Actor {
  def receive = ???
}

object ApiActor {

  /**
   * Starts the search. At the same time a string that identifies the status of the current search is created in the
   * database. As far as finding new tours, increases the value of the counter search.offersFound. When all providers
   * of tours were asked, the search goes into a status 'completed' (see DbActor.SearchStatus).
   *
   * Search finishes for a few seconds (5-20). During the processing of a single request you can run only one search.
   */
  case class StartSearch(query: SearchQuery)
  case class SearchStarted(searchId: String) // в ответ на StartSearch

  /**
   * Start the download the detailed information about flights for a given tour. After finishing the download, it
   * updates the information about the tour in NoSQL database. I.e. it is necessary to periodically download a Tour
   * from NoSQL database to wait for the moment when fields of tour 'departFlights' and 'arrivalFlights' will be filled.
   *
   * Download finishes for a few seconds (5-20). During the processing of a single request you can run multiple
   * downloads of flights (but preferably less).
   */
  case class StartFlightLoading(tourId: String)

  /** In response to StartFlightLoading, the receipt of this message doesn't indicate that the flight is loaded - only
   * that their download has started
   */
  case class FlightLoadingStarted(tourId: String)
}
