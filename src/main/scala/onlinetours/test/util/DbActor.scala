package onlinetours.test.util

import akka.actor.Actor

/**
 * Interacts with a relational database. All information necessary for the service is stored in the database (except
 * for tours that are stored in a NoSQL database, see the actor ToursDbActor)
 */
class DbActor extends Actor {
  def receive = ???
}

object DbActor {

  case class FetchRegionIdByIata(regionIatas: Traversable[String])
  case class RegionByIataFetched(regionIdByIata: Map[String, Long]) // response to FetchRegionIdByIata

  case class FetchAviacompanyIataByName(aviacompanies: Traversable[String])
  case class AviacompanyIataByNameFetched(iataByName: Map[String, String]) // response to FetchAviacompanyIataByName

  /**
   * When the search is started (see ApiActor.StartSearch), you can check the status of the search using searchId
   */
  case class FetchSearchStatus(searchId: String)
  /**
   * @param searchId ID of the search
   * @param completed if completed=true, then search is completed and the new tours that meet the search criteria will
   *                  no longer get into database
   * @param toursFound number of tours found and stored in a NoSQL database in the context of the search with id=searchId
   */
  case class SearchStatus(searchId: String, completed: Boolean, toursFound: Int) // response to FetchSearchStatus
}
