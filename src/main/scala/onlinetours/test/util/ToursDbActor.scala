package onlinetours.test.util

import akka.actor.Actor
import onlinetours.test.Flights.{Tour, SearchQuery}
import org.joda.time.{LocalTime, LocalDate}

/**
 * Взаимодействует с NoSQL БД, в которой хранятся все найденные туры
 * БД автоматически удаляет устаревшие туры
 */
class ToursDbActor extends Actor {
  def receive = ???
}

object ToursDbActor {

  /**
   * Find the cheapest tour that satisfies the search query
   */
  case class FindCheapestTour(query: SearchQuery)

  /**
   * Response to FindCheapestTour.
   * I.e. tours may not be available for a given search query in the database
   */
  case class Cheapest(tour: Option[DbTour])

  /**
   * Download the specific tour. For example, to check tour flight information
   */
  case class FetchTour(tourId: String)

  /**
   * Tours can be not found in a NoSQL database, for example if the tour was deleted from the database, as obsolete
   * (occurs half an hour after its creation).
   */
  case class TourFetched(tour: Option[DbTour])

  case class DbTour(tourId: String, hotelName: String, minCost: Int,
                    departFlights: Option[Traversable[DbFlight]], arrivalFlights: Option[Traversable[DbFlight]])
  case class DbFlight(aviacompanyName: String, date: LocalDate, time: Option[LocalTime])
}
