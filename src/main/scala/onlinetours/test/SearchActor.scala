package onlinetours.test

import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import onlinetours.test.Flights._
import onlinetours.test.util.ApiActor.{SearchStarted, StartFlightLoading, StartSearch}
import onlinetours.test.util.DbActor._
import onlinetours.test.util.ToursDbActor._
import org.joda.time.{LocalTime, Days}

import scala.concurrent.duration._
import scala.util.{Failure, Success}

/**
 * The actor that you need to implement
 */
class SearchActor(request: Request, requester: ActorRef, db: ActorRef, tourDb: ActorRef, api: ActorRef) extends Actor with ActorLogging {

  import SearchActor._

  private val deadline = (RequestTimeout - TickInterval - RespondReserve).fromNow
  private implicit val exec = context.dispatcher
  private var dbToursByAviaCompanies = Map.empty[Set[String], Set[DbTour]]
  private var tourCandidate: Option[Tour] = None
  private var searchId: Option[String] = None
  private var loadingTours = Set.empty[String]
  private val iataCodes = Set(request.arrivalIata, request.departureIata)
  private var searchCompleted = false
  private var searchQuery: SearchQuery = _

  fetchRegionId(iataCodes) onComplete {
    case Success(resp) =>
      self ! resp
    case Failure(ex) =>
      handleError(Some(ex), "Failed to fetch iata codes")
  }

  def receive: Receive = {
    case RegionByIataFetched(regIdByIata) =>
      val notFoundIata = iataCodes.diff(regIdByIata.keySet)
      if (notFoundIata.isEmpty) {
        searchQuery = SearchQuery(
          departure = regIdByIata(request.departureIata),
          arrival = regIdByIata(request.arrivalIata),
          startDate = request.start,
          duration = Days.daysBetween(request.start, request.end).getDays
        )

        context.become(searching)

        checkTourCache
        startApiSearch

        context.system.scheduler.scheduleOnce(TickInterval, self, Tick)
      } else {
        handleError(msg = s"Iata codes not found: ${notFoundIata.mkString(", ")}")
      }
  }

  private def searching: Receive = {
    case SearchStarted(id) =>
      log.debug(s"searchId is received: '$id'")
      searchId = Some(id)

    case Cheapest(t@Some(tour)) =>
      (tour.departFlights, tour.arrivalFlights) match {
        case (Some(_), Some(_)) =>
          tourLoaded(tour)
        case _ =>
          if (!loadingTours.contains(tour.tourId)) {
            log.debug(s"Start loading flight info for tour ${tour.tourId}")
            loadingTours = loadingTours + tour.tourId
            api ! StartFlightLoading(tour.tourId)
          }
      }

    case AviacompanyIataByNameFetched(iataByName) =>
      dbToursByAviaCompanies.get(iataByName.keySet) foreach { dbTours =>
        dbTours foreach { dbTour =>
          if (isCheaperThanCurrent(dbTour)) {
            val tour = Tour(
              tourId = dbTour.tourId,
              hotelName = dbTour.hotelName,
              minCost = dbTour.minCost,
              departFlights = dbTour.departFlights.get.map { dbFlight =>
                Flight(
                  aviacompany = Aviacompany(name = dbFlight.aviacompanyName, iata = iataByName.get(dbFlight.aviacompanyName)),
                  date = dbFlight.date.toLocalDateTime(dbFlight.time.getOrElse(DefaultLocalTime))
                )
              },
              arrivalFlights = dbTour.arrivalFlights.get.map { dbFlight =>
                Flight(
                  aviacompany = Aviacompany(name = dbFlight.aviacompanyName, iata = iataByName.get(dbFlight.aviacompanyName)),
                  date = dbFlight.date.toLocalDateTime(dbFlight.time.getOrElse(DefaultLocalTime))
                )
              }
            )

            log.debug(s"Found new cheapest tour '${tour.tourId}'")

            tourCandidate = Some(tour)
          }
        }

        dbToursByAviaCompanies = dbToursByAviaCompanies - iataByName.keySet
      }

    case SearchStatus(_, true, n) if !searchCompleted =>
      searchCompleted = true
      log.debug(s"Search '$searchId' completed. Found $n tours")
      if (n == 0 && timeToRespond) {
        respond()
      } else if (n > 0) {
        checkTourCache
      }

    case TourFetched(Some(tour)) =>
      if (tour.arrivalFlights.isDefined && tour.departFlights.isDefined) {
        tourLoaded(tour)
      }

    case Tick =>
      if (timeToRespond) {
        respond()
      } else {
        if (!searchCompleted) {
          fetchSearchStatus
          checkTourCache
        }

        implicit val timeout: Timeout = ToursDbTimeout
        loadingTours foreach { tour =>
          tourDb ? FetchTour(tour) pipeTo self
        }

        context.system.scheduler.scheduleOnce(TickInterval, self, Tick)
      }
  }

  private def isCheaperThanCurrent(dbTour: DbTour): Boolean =
    tourCandidate.isEmpty || dbTour.minCost < tourCandidate.get.minCost

  private def fetchSearchStatus =
    searchId foreach { id =>
      implicit val timeout: Timeout = DbTimeout
      db ? FetchSearchStatus(id) pipeTo self
    }

  private def tourLoaded(tour: DbTour) = {
    loadingTours = loadingTours - tour.tourId
    if (isCheaperThanCurrent(tour)) {
      log.debug(s"Got new tour candidate '${tour.tourId}'")
      val aviaCompanies = (tour.arrivalFlights.get ++ tour.departFlights.get).map(_.aviacompanyName).toSet
      val newValue = if (dbToursByAviaCompanies.contains(aviaCompanies)) {
        dbToursByAviaCompanies(aviaCompanies) + tour
      } else {
        Set(tour)
      }
      dbToursByAviaCompanies = dbToursByAviaCompanies.updated(aviaCompanies, newValue)

      implicit val timeout: Timeout = DbTimeout
      db ? FetchAviacompanyIataByName(aviaCompanies) pipeTo self
    }
  }

  private def timeToRespond: Boolean =
    deadline.isOverdue() || (searchCompleted && loadingTours.isEmpty && dbToursByAviaCompanies.isEmpty)

  private def respond(): Unit = {
    val response = tourCandidate match {
      case Some(tour) =>
        Response(Success(tour))
      case None =>
        Response(Failure(new RuntimeException("Failed to find tours")))
    }

    requester ! response
    context.stop(self)
  }

  private def checkTourCache = {
    implicit val timeout: Timeout = ToursDbTimeout
    tourDb ? FindCheapestTour(searchQuery) pipeTo self
  }

  private def startApiSearch = {
    implicit val timeout: Timeout = ApiTimeout
    api ? StartSearch(searchQuery) pipeTo self
  }

  private def handleError(err: Option[Throwable] = None, msg: String) = {
    if (err.isDefined)
      log.error(err.get, msg)
    requester ! Response(Failure(new RuntimeException(msg)))
    context.stop(self)
  }

  private def fetchRegionId(regionIatas: Traversable[String]) = {
    implicit val timeout: Timeout = DbTimeout
    db ? FetchRegionIdByIata(regionIatas)
  }
}


object SearchActor {
  val RequestTimeout = 25.seconds
  val DbTimeout = 2.seconds
  val ToursDbTimeout = 1.seconds
  val ApiTimeout = 2.second
  val TickInterval = 1.second
  val RespondReserve = 100.millis

  val DefaultLocalTime = LocalTime.MIDNIGHT

  case object Tick
}
