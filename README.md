### Application for finding the cheapest round-trip flight with a hotel stay

You need to give the cheapest possible result within 30 seconds or reply with any available result. 
I.e. no result is worse than to reply with not the cheapest result.

Actors DbActor, ToursDbActor, ApiActor can be considered as already implemented. Their protocols are described in the 
relevant companion objects.
* DbActor interacts with a relational database.
* ToursDbActor interacts with NoSQL database that stores all the found tours. Tours are returned as objects of 
  class DbTour.
* ApiActor interacts with site API (common for this service and for js-scripts site), knows how to start the search 
  and start loading detailed information about flights. Tours found through the search never have detailed information 
  about flights. The flight information is obtained by a separate request (see ApiActor.StartFlightLoading)

You must implement the actor SearchActor, that performs search. The sequence of operation is as follows:

1. In the constructor actor SearchActor receives Flights.Request (and references to the above actors, which are 
responsible for communication with other services).

2. Flights.Request contains the iata-codes for the cities of departure/arrival, however, all services work with the 
identifiers of the regions so you need to convert them to IDs using DbActor.FetchRegionIdByIata.

3. DbTour objects are stored in NoSQL database, including data obtained from the previous search (i.e., a cache of 
tours). So it makes sense to find the cheapest tour in the cache.

4. Tour from the cache can both contain and not contain detailed information about the flight (airline, airport, time, 
etc.), and more often than not contains. Here and further: the tour does not contain information about the flight, if 
its fields departFlights, arrivalFlights equal to None.

5. Simultaneously with the search in the cache, you should start search for tours through ApiActor.StartSearch.

6. Found through search tours are also stored in a NoSQL database and can be queried via ToursDbActor.FindCheapestTour.
If the cheapest tour, that was found in the search process, is cheaper than the tour, which had been obtained directly 
from the cache, then this tour should be in the response (after performing the downloading of information about the 
flight, as described above).

7. Before answering with the response, you need to translate the names of the airlines in their iata-codes (see 
DbActor.FetchAviacompanyIataByName).

8. Send a message Flights.Response with found tour to the requester. If the request processing errors occurred, making 
the processing of the request is impossible, it should send an error to the requester via the same message and stop the 
actor SearchActor.